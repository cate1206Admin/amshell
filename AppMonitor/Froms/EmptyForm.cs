﻿using AppMonitor.Bex;
using AppMonitor.Model;
using AppMonitor.Plugin;
using AppMonitor.Plugin.Zookeeper;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AppMonitor.Froms
{
    public partial class EmptyForm : MonitorBaseForm
    {
        public EmptyForm()
        {
            InitializeComponent();
        }

        public override void CloseForm()
        {
            this.Close();
        }

        private void EmptyForm_Load(object sender, EventArgs e)
        {
            
        }

        public override void ReflushMonitorItem()
        {
            
        }


        private void EmptyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void EmptyForm_ResizeEnd(object sender, EventArgs e)
        {
            label1.Location = new Point(this.Size.Width/2, this.Size.Height/2);
        }

    }
}
